package at.ac.tuwien.inso.sqm.repository;

import at.ac.tuwien.inso.sqm.entity.Subjcet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends CrudRepository<Subjcet, Long> {

    List<Subjcet> findByLecturersId(Long id);

    Subjcet findById(Long id);

    List<Subjcet> findByNameContainingIgnoreCase(String name);

    Page<Subjcet> findAll(Pageable pageable);

    Page<Subjcet> findAllByNameLikeIgnoreCase(String name, Pageable pageable);
}
