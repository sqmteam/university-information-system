package at.ac.tuwien.inso.sqm.controller.admin.forms.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueIdentificationNumberConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueIdentificationNumber {

    String message() default "{UniqueIdentificationNumber}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}